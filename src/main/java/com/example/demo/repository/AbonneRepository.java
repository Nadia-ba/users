package com.example.demo.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.models.Abonne;

@Repository
public interface AbonneRepository extends CrudRepository<Abonne, Long> {

    
    Abonne findByEmail(String email);

}
