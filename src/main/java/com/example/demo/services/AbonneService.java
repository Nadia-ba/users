package com.example.demo.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.models.Abonne;
import com.example.demo.repository.AbonneRepository;

@Service
public class AbonneService {

	@Autowired
	private AbonneRepository abonneRepository;
	
	public Iterable<Abonne> getAbonnes(){
		return abonneRepository.findAll();
	}
	
	public Abonne getAbonneById(long id) {
		Abonne A = abonneRepository.findById(id).get();
		return A;
	}
	public Abonne getAbonneByEmail(String email){
		return  abonneRepository.findByEmail(email);
	}
	
	public void deletAbonneById(long id) {
		abonneRepository.deleteById(id);
	}
	
	public Abonne addAbonne(Abonne abonne) {
		return abonneRepository.save(abonne);
	}

	public Iterable<Abonne> addAbonnes(Iterable<Abonne> abonnes){
		return abonneRepository.saveAll( abonnes);
	}
}
