package com.example.demo.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.models.Abonne;
import com.example.demo.services.AbonneService;

@RestController
public class AbonneController {
	
	@Autowired
	AbonneService abonneService;
	@CrossOrigin
	@GetMapping("/Abonnes")
	public Iterable<Abonne> getAbonnes(){
		return abonneService.getAbonnes();
	}
	
	@CrossOrigin
	@GetMapping("/Abonne/{id}")
	public Abonne getAbonneById(@org.springframework.web.bind.annotation.PathVariable long id) {
		return abonneService.getAbonneById(id);
	}
	
	@CrossOrigin
	@PostMapping("/addAbonne")
	public Abonne addNewAbonne(@RequestBody Abonne abonne) {
		
		return abonneService.addAbonne(abonne);
	}

	@CrossOrigin
	@DeleteMapping("delete/{id}")
	public void deleteAbonne(@PathVariable long id) {
		abonneService.deletAbonneById(id);
	}

	@CrossOrigin
	@PostMapping("/addabonnes")
	public Iterable<Abonne> addUsers(@RequestBody Iterable<Abonne> abonnes){
		return abonneService.addAbonnes(abonnes);
	}
	

}
